package main

import (
	"bytes"
	"flag"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/fsnotify/fsnotify"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

func prometheusHandler() http.Handler {
	return prometheus.Handler()
}

type Addresses struct {
	Increment int
	Run       int
}

var start, executionTime, increment, run int
var url, parameter, query string
var tmpl *template.Template

func main() {
	viper.SetConfigType("yaml")
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s", err))
	}

	flag.Int("start", 10, "Number of threads to start with")
	flag.Int("executionTime", 60, "Time for each iteration in seconds")
	flag.String("url", "https://www.google.com", "URL to call")
	flag.Int("run", 1, "Run Number")
	flag.String("query", "", "Query to add")

	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)

	start = viper.GetInt("start")
	executionTime = viper.GetInt("executionTime")
	url = viper.GetString("url")
	run = viper.GetInt("run")
	query = viper.GetString("query")

	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
		err := viper.ReadInConfig() // Find and read the config file
		if err != nil {             // Handle errors reading the config file
			fmt.Printf("Error in config file: %s", err)
		}
		start = viper.GetInt("start")
		executionTime = viper.GetInt("executionTime")
		url = viper.GetString("url")
		run = viper.GetInt("run")
		query = viper.GetString("query")
	})

	histogram := prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: "request_time",
		Help: "Time taken for requests",
	}, []string{"code"})

	r := mux.NewRouter()
	r.Handle("/metrics", prometheusHandler())
	r.Handle("/initiate", initiateHandler(histogram))

	prometheus.Register(histogram)

	s := &http.Server{
		Addr:           ":8080",
		ReadTimeout:    8 * time.Second,
		WriteTimeout:   8 * time.Second,
		MaxHeaderBytes: 1 << 20,
		Handler:        r,
	}

	fmt.Println(start)
	fmt.Println(executionTime)
	fmt.Println(url)
	fmt.Println(run)
	fmt.Println(query)

	tmpl, err = template.New("test").Parse(query)
	if err != nil {
		panic(fmt.Errorf("Fatal parse of parameter: %s", err))
	}
	increment = 0
	s.ListenAndServe()
}

func initiateHandler(histogram *prometheus.HistogramVec) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		val := fmt.Sprintf("%s\n", "Tests running with the following settings:")
		val = val + fmt.Sprintf("%s:\t%d\n", "Starting thread count", start)
		val = val + fmt.Sprintf("%s:\t%d\n", "Test execution time", executionTime)
		val = val + fmt.Sprintf("%s:\t%s\n", "URL to connect to", url)
		w.Write([]byte(val))

		var wg sync.WaitGroup
		threads := start
		for n := 0; n < executionTime; n++ {
			wg.Add(threads)
			for j := 0; j < threads; j++ {
				go callURL(url, histogram, &wg, increment)
				increment++
			}
			time.Sleep(1 * time.Second)
		}
		wg.Wait()
	}
}

func callURL(url string, histogram *prometheus.HistogramVec, wg *sync.WaitGroup, increment int) {
	var tpl bytes.Buffer
	var newURL string
	err := tmpl.Execute(&tpl, Addresses{increment, run})
	if err != nil {
		fmt.Printf("Fatal execute of parameter: %s", err)
		newURL = url
	} else {
		result := tpl.String()
		newURL = url + "?" + result
	}

	startTime := time.Now()
	code := 500

	defer func() {
		duration := time.Since(startTime)
		histogram.WithLabelValues(fmt.Sprintf("%d", code)).Observe(duration.Seconds())
		wg.Done()
	}()

	resp, err := http.Get(newURL)
	if err != nil {
		log.Printf("Fatal error in request: %s", err)
	} else {
		code = resp.StatusCode
		defer resp.Body.Close()
	}
}
